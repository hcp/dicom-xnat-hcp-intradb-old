/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.xnat.dcm;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

import org.apache.jmeter.samplers.Entry;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.jmeter.samplers.Sampler;
import org.dcm4che2.data.DicomObject;
import org.dcm4che2.data.Tag;
import org.dcm4che2.data.TransferSyntax;
import org.dcm4che2.net.NetworkApplicationEntity;
import org.dcm4che2.net.NetworkApplicationEntityBuilder;
import org.dcm4che2.net.NetworkConnection;
import org.dcm4che2.net.NetworkConnectionBuilder;
import org.dcm4che2.net.TransferCapability;
import org.nrg.dcm.DicomSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class DicomStudyCStoreSampler
extends MockDicomStudyAbstractSampler implements Sampler {
    private static final long serialVersionUID = 1L;
    public static final String SAMPLER_TITLE = "DICOM C-STORE to XNAT";
    private static final String DEFAULT_AE_TITLE = "XNAT-JMeter";
    private static final String DEFAULT_TRANSFER_SYNTAX = TransferSyntax.ImplicitVRLittleEndian.uid();

    private final Logger logger = LoggerFactory.getLogger(DicomStudyCStoreSampler.class);

    /**
     * @param name
     */
    public DicomStudyCStoreSampler() {
        super(SAMPLER_TITLE);
    }

    private TransferCapability[] buildTransferCapabilities() throws IOException {
        final DicomObject o = makeTemplateObject("1", 1, "1", 1, "1");
        return new TransferCapability[] {
                new TransferCapability(o.getString(Tag.SOPClassUID),
                        new String[] {
                    o.getString(Tag.TransferSyntaxUID, DEFAULT_TRANSFER_SYNTAX),
                },
                TransferCapability.SCU)
        };
    }

    private static DicomSender buildSender(final String remHost, final int remPort,
            final String remAETitle, final String locAETitle,
            final TransferCapability[] tcs) {
        final NetworkConnection lnc = new NetworkConnection();

        final NetworkApplicationEntity localAE = new NetworkApplicationEntityBuilder()
        .setAETitle(null == locAETitle ? DEFAULT_AE_TITLE : locAETitle)
        .setTransferCapability(tcs)
        .setNetworkConnection(lnc)
        .build();

        final NetworkConnectionBuilder rncb = new NetworkConnectionBuilder();
        rncb.setHostname(remHost).setPort(remPort);

        final NetworkApplicationEntity remoteAE = new NetworkApplicationEntityBuilder()
        .setNetworkConnection(rncb.build())
        .setAETitle(remAETitle)
        .build();

        return new DicomSender(localAE, remoteAE);
    }

    /* (non-Javadoc)
     * @see org.apache.jmeter.samplers.Sampler#sample(org.apache.jmeter.samplers.Entry)
     */
    public SampleResult sample(final Entry entry) {
        setAuthenticator();
        final URI importURL = getURI();
        if (! "dicom".equals(importURL.getScheme())) {
            throw new IllegalArgumentException("destination URL protocol must be dicom");
        }

        final int[] counts = getCountsArray();

        final SampleResult result = new SampleResult();
        result.setSampleLabel(SAMPLER_TITLE);
        result.setDataType(SampleResult.TEXT);
        result.setSamplerData(Arrays.toString(counts));

        result.sampleStart();
        try {
            final DicomSender sender = buildSender(importURL.getHost(), importURL.getPort(),
                    importURL.getPath().replaceFirst("/", ""),
                    importURL.getUserInfo(), buildTransferCapabilities());

            final String studyInstanceUID = getNewUID();
            for (int series = 0; series < counts.length; series++) {
                final String seriesInstanceUID = getNewUID();
                for (int instance = 0; instance < counts[series]; instance++) {
                    final String sopInstanceUID = getNewUID();
                    final DicomObject o = makeTemplateObject(studyInstanceUID,
                            series, seriesInstanceUID,
                            instance, sopInstanceUID);
                    sender.send(o, o.getString(Tag.TransferSyntaxUID, DEFAULT_TRANSFER_SYNTAX));
                }
            }
            result.setResponseOK();
            result.setSuccessful(true);

            long size = 0;
            int totalCount = 0;
            for (int seriesCount : counts) {
                size += seriesCount * getTemplateObjectSize();
                totalCount += seriesCount;
            }
            result.setBytes((int)size);
            result.setSampleCount(totalCount);
        } catch (Throwable t) {
            System.err.println("failed to send DICOM file");
            t.printStackTrace(System.err);
            logger.info("unable to send DICOM file", t);
            result.setSuccessful(false);
        } finally {
            result.sampleEnd();
        }
        return result;
    }
}
