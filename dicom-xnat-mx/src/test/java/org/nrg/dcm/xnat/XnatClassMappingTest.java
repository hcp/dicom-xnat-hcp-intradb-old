/**
 * Copyright 2010 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import org.junit.Test;
import org.nrg.xdat.bean.XnatCrscandataBean;
import org.nrg.xdat.bean.XnatCrsessiondataBean;
import org.nrg.xdat.bean.XnatCtscandataBean;
import org.nrg.xdat.bean.XnatCtsessiondataBean;
import org.nrg.xdat.bean.XnatDx3dcraniofacialscandataBean;
import org.nrg.xdat.bean.XnatDx3dcraniofacialsessiondataBean;
import org.nrg.xdat.bean.XnatEcgscandataBean;
import org.nrg.xdat.bean.XnatEcgsessiondataBean;
import org.nrg.xdat.bean.XnatEpsscandataBean;
import org.nrg.xdat.bean.XnatEpssessiondataBean;
import org.nrg.xdat.bean.XnatEsvscandataBean;
import org.nrg.xdat.bean.XnatEsvsessiondataBean;
import org.nrg.xdat.bean.XnatGmvscandataBean;
import org.nrg.xdat.bean.XnatGmvsessiondataBean;
import org.nrg.xdat.bean.XnatHdscandataBean;
import org.nrg.xdat.bean.XnatHdsessiondataBean;
import org.nrg.xdat.bean.XnatIoscandataBean;
import org.nrg.xdat.bean.XnatIosessiondataBean;
import org.nrg.xdat.bean.XnatMgscandataBean;
import org.nrg.xdat.bean.XnatMgsessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.XnatNmscandataBean;
import org.nrg.xdat.bean.XnatNmsessiondataBean;
import org.nrg.xdat.bean.XnatOpscandataBean;
import org.nrg.xdat.bean.XnatOpsessiondataBean;
import org.nrg.xdat.bean.XnatOptscandataBean;
import org.nrg.xdat.bean.XnatOptsessiondataBean;
import org.nrg.xdat.bean.XnatOtherdicomscandataBean;
import org.nrg.xdat.bean.XnatOtherdicomsessiondataBean;
import org.nrg.xdat.bean.XnatPetscandataBean;
import org.nrg.xdat.bean.XnatPetsessiondataBean;
import org.nrg.xdat.bean.XnatRfscandataBean;
import org.nrg.xdat.bean.XnatRfsessiondataBean;
import org.nrg.xdat.bean.XnatRtimagescandataBean;
import org.nrg.xdat.bean.XnatRtsessiondataBean;
import org.nrg.xdat.bean.XnatScscandataBean;
import org.nrg.xdat.bean.XnatUsscandataBean;
import org.nrg.xdat.bean.XnatUssessiondataBean;
import org.nrg.xdat.bean.XnatXascandataBean;
import org.nrg.xdat.bean.XnatXasessiondataBean;
import org.nrg.xdat.bean.XnatXcvscandataBean;
import org.nrg.xdat.bean.XnatXcvsessiondataBean;
import org.nrg.xdat.bean.base.BaseElement;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class XnatClassMappingTest {

	/**
	 * Test method for {@link org.nrg.dcm.xnat.XnatClassMapping#getBeanClass(java.lang.String)}.
	 */
	@Test
	public void testgetBeanClass() throws ClassNotFoundException {
		assertEquals(XnatMrscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("mrScanData"));
		assertEquals(XnatCrscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("crScanData"));
		assertEquals(XnatCtscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("ctScanData"));
		assertEquals(XnatDx3dcraniofacialscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("dx3DCraniofacialScanData"));
		assertEquals(XnatEcgscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("ecgScanData"));
		assertEquals(XnatEpsscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("epsScanData"));
		assertEquals(XnatEsvscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("esvScanData"));
		assertEquals(XnatGmvscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("gmvScanData"));
		assertEquals(XnatHdscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("hdScanData"));
		assertEquals(XnatIoscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("ioScanData"));
		assertEquals(XnatMgscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("mgScanData"));
		assertEquals(XnatNmscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("nmScanData"));
		assertEquals(XnatOpscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("opScanData"));
		assertEquals(XnatMgscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("mgScanData"));
		assertEquals(XnatOptscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("optScanData"));
		assertEquals(XnatOtherdicomscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("otherDicomScanData"));
		assertEquals(XnatPetscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("petScanData"));
		assertEquals(XnatRfscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("rfScanData"));
		assertEquals(XnatRtimagescandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("rtImageScanData"));
		assertEquals(XnatScscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("scScanData"));
		assertEquals(XnatUsscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("usScanData"));
		assertEquals(XnatXascandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("xaScanData"));
		assertEquals(XnatXcvscandataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("xcvScanData"));
		
		assertEquals(XnatMrsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("mrSessionData"));
		assertEquals(XnatCrsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("crSessionData"));
		assertEquals(XnatCtsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("ctSessionData"));
		assertEquals(XnatDx3dcraniofacialsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("dx3DCraniofacialSessionData"));
		assertEquals(XnatEcgsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("ecgSessionData"));
		assertEquals(XnatEpssessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("epsSessionData"));
		assertEquals(XnatEsvsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("esvSessionData"));
		assertEquals(XnatGmvsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("gmvSessionData"));
		assertEquals(XnatHdsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("hdSessionData"));
		assertEquals(XnatIosessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("ioSessionData"));
		assertEquals(XnatMgsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("mgSessionData"));
		assertEquals(XnatNmsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("nmSessionData"));
		assertEquals(XnatOpsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("opSessionData"));
		assertEquals(XnatMgsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("mgSessionData"));
		assertEquals(XnatOptsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("optSessionData"));
		assertEquals(XnatOtherdicomsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("otherDicomSessionData"));
		assertEquals(XnatPetsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("petSessionData"));
		assertEquals(XnatRfsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("rfSessionData"));
		assertEquals(XnatRtsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("rtSessionData"));
		assertEquals(XnatUssessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("usSessionData"));
		assertEquals(XnatXasessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("xaSessionData"));
		assertEquals(XnatXcvsessiondataBean.class, XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("xcvSessionData"));
	}
	
	@Test(expected=ClassNotFoundException.class)
	public void testNoSuchBeanClass() throws ClassNotFoundException {
		XnatClassMapping.forBaseClass(BaseElement.class).getBeanClass("scSessionData");
	}
}
