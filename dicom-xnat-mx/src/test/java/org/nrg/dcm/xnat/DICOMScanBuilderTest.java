/**
 * Copyright 2010,2011 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.taskdefs.Delete;
import org.apache.tools.ant.types.FileSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.dcm.DicomAttributeIndex;

import static org.nrg.dcm.Attributes.*;

import org.nrg.xdat.bean.XnatImagescandataBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;

import com.google.common.collect.ImmutableSet;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class DICOMScanBuilderTest {
    private static final File sample1dir = new File(System.getProperty("sample.data.dir"));
    private static final String scan4_includes = "1.MR.head_DHead.4.*";
    private File scan4Dir = null;

    @Before
    public void setUp() throws IOException {
        if (null != scan4Dir) {
            throw new IllegalStateException("scan directory already assigned");
        }
        scan4Dir = File.createTempFile("org.nrg.dcm.xnat.DICOMScanBuilder", ".test");
        scan4Dir.delete();
        scan4Dir.mkdir();

        final Project project = new Project();
        project.setBaseDir(scan4Dir);
        final Copy copy = new Copy();
        copy.setProject(project);
        copy.setTodir(scan4Dir);

        final FileSet scan4 = new FileSet();
        scan4.setIncludes(scan4_includes);
        scan4.setDir(sample1dir);

        copy.addFileset(scan4);
        copy.execute();
    }

    @After
    public void tearDown() {
        if (null == scan4Dir) {
            throw new IllegalStateException("scan directory not assigned");
        }
        final Project project = new Project();
        project.setBaseDir(scan4Dir);
        final Delete delete = new Delete();
        delete.setDir(scan4Dir);
        delete.setVerbose(false);
        delete.execute();

        scan4Dir = null;
    }
    
    /**
     * Test method for {@link org.nrg.dcm.xnat.DICOMScanBuilder#getNativeTypeAttrs()}.
     */
    @Test
    public void testGetNativeTypeAttrs() {
        final ImmutableSet<DicomAttributeIndex> attrs = DICOMScanBuilder.getNativeTypeAttrs();
        assertTrue(attrs.contains(SeriesInstanceUID));
        assertFalse(attrs.contains(StudyInstanceUID));
    }

    @Test
    public void testFromScanDirectoryWithRelativePaths() throws IOException,SQLException,URISyntaxException {
        final XnatImagescandataBean scan = DICOMScanBuilder.buildScanFromDirectory(scan4Dir, scan4Dir, "4", true);
        final XnatResourcecatalogBean catResource = (XnatResourcecatalogBean)scan.getFile().get(0);
        assertEquals("DICOM", catResource.getFormat());
        assertEquals("DICOM", catResource.getLabel());
        assertEquals("RAW", catResource.getContent());
        final String catalogPath = catResource.getUri();
        assertFalse(catalogPath.startsWith("/"));
        final File catalogFile = new File(scan4Dir, catalogPath);
        assertTrue(catalogFile.exists());
    }

    @Test
    public void testFromScanDirectoryWithAbsolutePaths() throws IOException,SQLException,URISyntaxException {
        final XnatImagescandataBean scan = DICOMScanBuilder.buildScanFromDirectory(scan4Dir, scan4Dir, "4", false);
        final XnatResourcecatalogBean catResource = (XnatResourcecatalogBean)scan.getFile().get(0);
        assertEquals("DICOM", catResource.getFormat());
        assertEquals("DICOM", catResource.getLabel());
        assertEquals("RAW", catResource.getContent());
        final String catalogPath = catResource.getUri();
        assertTrue(catalogPath.startsWith("/"));
        final File catalogFile = new File(catalogPath);
        assertTrue(catalogFile.exists());
    }
}
