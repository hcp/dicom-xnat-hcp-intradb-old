/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Copy;
import org.apache.tools.ant.taskdefs.Delete;
import org.apache.tools.ant.types.FileSet;
import org.dcm4che2.data.Tag;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.dcm.EnumeratedMetadataStore;
import org.nrg.dcm.FixedDicomAttributeIndex;
import org.nrg.dcm.SOPModel;
import org.nrg.xdat.bean.CatDcmcatalogBean;
import org.nrg.xdat.bean.XnatResourcecatalogBean;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class CatalogBuilderTest {
	private static final File sample1dir = new File(System.getProperty("sample.data.dir"));
	private static final String scan4_includes = "1.MR.head_DHead.4.*";
	private File scan4Dir = null;
	
	private static final Collection<DicomAttributeIndex> TAGS;
	static {
		final Collection<DicomAttributeIndex> tags = Lists.newArrayList();
		tags.add(new FixedDicomAttributeIndex(Tag.SOPClassUID));
		tags.addAll(ImageFileAttributes.get().getNativeAttrs());
		tags.addAll(CatalogAttributes.get().getNativeAttrs());
		TAGS = ImmutableList.copyOf(tags);
	}
	
	private static final Collection<String> ADD_COLS = Arrays.asList(SOPModel.XNAT_SCAN_COLUMN);
	
	@Before
	public void setUp() throws IOException {
		if (null != scan4Dir) {
			throw new IllegalStateException("scan directory already assigned");
		}
		scan4Dir = File.createTempFile("org.nrg.dcm.xnat.CatalogBuilder", ".test");
		scan4Dir.delete();
		scan4Dir.mkdir();
		
		final Project project = new Project();
		project.setBaseDir(scan4Dir);
		final Copy copy = new Copy();
		copy.setProject(project);
		copy.setTodir(scan4Dir);
		
		final FileSet scan4 = new FileSet();
		scan4.setIncludes(scan4_includes);
		scan4.setDir(sample1dir);
		
		copy.addFileset(scan4);
		copy.execute();
	}

	@After
	public void tearDown() {
		if (null == scan4Dir) {
			throw new IllegalStateException("scan directory not assigned");
		}
		final Project project = new Project();
		project.setBaseDir(scan4Dir);
		final Delete delete = new Delete();
		delete.setDir(scan4Dir);
		delete.setVerbose(false);
		delete.execute();
		
		scan4Dir = null;
	}

	@Test
	public void testCall() throws IOException,SQLException {
		final DicomMetadataStore store = EnumeratedMetadataStore.createHSQLDBBacked(TAGS);
		store.add(Collections.singleton(scan4Dir));
		final CatalogBuilder builder = new CatalogBuilder("4", null, store, scan4Dir, false);
		final Map<XnatResourcecatalogBean,CatDcmcatalogBean> catalogs = builder.call();
		assertEquals(1, catalogs.size());
		final Map.Entry<XnatResourcecatalogBean,CatDcmcatalogBean> me = catalogs.entrySet().iterator().next();
		final XnatResourcecatalogBean resource = me.getKey();
		assertEquals("DICOM", resource.getLabel());
		assertEquals("DICOM", resource.getFormat());
		assertEquals("RAW", resource.getContent());

		final CatDcmcatalogBean catalog = me.getValue();
		assertEquals(Integer.valueOf(176), catalog.getDimensions_z());
		assertEquals(Integer.valueOf(256), catalog.getDimensions_x());
		assertEquals(Integer.valueOf(256), catalog.getDimensions_y());
	}
	
	@Test
	public void testCallWithRead() throws IOException,SQLException {
		final DicomMetadataStore store = EnumeratedMetadataStore.createHSQLDBBacked(TAGS, ADD_COLS);
		final CatalogBuilder builder = new CatalogBuilder("4", null, store, scan4Dir, true);
		final Map<XnatResourcecatalogBean,CatDcmcatalogBean> catalogs = builder.call();
		assertEquals(1, catalogs.size());
		final Map.Entry<XnatResourcecatalogBean,CatDcmcatalogBean> me = catalogs.entrySet().iterator().next();
		final XnatResourcecatalogBean resource = me.getKey();
		assertEquals("DICOM", resource.getLabel());
		assertEquals("DICOM", resource.getFormat());
		assertEquals("RAW", resource.getContent());

		final CatDcmcatalogBean catalog = me.getValue();
		assertEquals(Integer.valueOf(176), catalog.getDimensions_z());
		assertEquals(Integer.valueOf(256), catalog.getDimensions_x());
		assertEquals(Integer.valueOf(256), catalog.getDimensions_y());
	}

	// TODO: test with secondary files
	// TODO: test some failure recovery?
}
