/**
 * Copyright (c) 2012,2014 Washington University
 */
package org.hcp.dcm;

import static org.nrg.dcm.Attributes.SeriesDescription;
import static org.nrg.dcm.SiemensPrivateAttributes.SIEMENS_SWIPMEMBLOCK_ALFREE_0;
import static org.nrg.dcm.SiemensPrivateAttributes.SIEMENS_SWIPMEMBLOCK_ALFREE_16;

import java.util.Arrays;
import java.util.Map;

import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.ExtAttrException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.xnat.XnatAttrDef;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class PMCAttr extends Abstract<String[]> implements XnatAttrDef {
    public PMCAttr() {
        this("parameters/pmc");
    }

    public PMCAttr(final String name) {
        super(name, SeriesDescription, SIEMENS_SWIPMEMBLOCK_ALFREE_0, SIEMENS_SWIPMEMBLOCK_ALFREE_16);
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#apply(java.lang.Object)
     */
    public Iterable<ExtAttrValue> apply(final String[] a) throws ExtAttrException {
        final ImmutableList.Builder<ExtAttrValue> valsb = ImmutableList.builder();
        if (null != a) {
            assert 3 == a.length;
            if (!Strings.isNullOrEmpty(a[0])) {
                final String v;
                if (a[0].startsWith("T1w")) {
                    v = "3".equals(a[1]) ? "on" : "off";
                } else if (a[0].startsWith("T2w")) {
                    v = "2".equals(a[2]) ? "on" : "off";
                } else {
                    v = null;
                }
                if (!Strings.isNullOrEmpty(v)) {
                    valsb.add(new BasicExtAttrValue(getName(), v));
                }
            }
        }
        return valsb.build();
    }


    /* (non-Javadoc)
     * @see org.nrg.attr.EvaluableAttrDef#foldl(java.lang.Object, java.util.Map)
     */
    public String[] foldl(final String[] a, final Map<? extends DicomAttributeIndex, ? extends String> m)
    throws NoUniqueValueException {
        final String[] v = new String[] {
                m.get(SeriesDescription),
                m.get(SIEMENS_SWIPMEMBLOCK_ALFREE_0),
                m.get(SIEMENS_SWIPMEMBLOCK_ALFREE_16)
        };
        if (null == v || Arrays.equals(v, a)) {
            return a;
        } else if (null == a) {
            return v;
        } else {
            throw new NoUniqueValueException(getName(), new String[]{Arrays.toString(a), Arrays.toString(v)});
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.Foldable#start()
     */
    public String[] start() {
        return null;
    }
}
