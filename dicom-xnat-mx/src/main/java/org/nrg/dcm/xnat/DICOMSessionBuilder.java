/**
 * Copyright (c) 2008-2013 Washington University School of Medicine
 */
package org.nrg.dcm.xnat;

import static org.nrg.dcm.Attributes.AccessionNumber;
import static org.nrg.dcm.Attributes.Modality;
import static org.nrg.dcm.Attributes.PatientComments;
import static org.nrg.dcm.Attributes.PatientID;
import static org.nrg.dcm.Attributes.PatientName;
import static org.nrg.dcm.Attributes.SOPClassUID;
import static org.nrg.dcm.Attributes.SeriesInstanceUID;
import static org.nrg.dcm.Attributes.SeriesNumber;
import static org.nrg.dcm.Attributes.StudyComments;
import static org.nrg.dcm.Attributes.StudyDescription;
import static org.nrg.dcm.Attributes.StudyInstanceUID;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dcm4che2.data.DicomObject;
import org.hcp.dcm.HCPSeriesDescription;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.attr.Utils;
import org.nrg.dcm.AttrAdapter;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.dcm.EnumeratedMetadataStore;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.dcm.SOPModel;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.EqualsRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;
import org.nrg.io.RelativePathWriterFactory;
import org.nrg.io.ScanCatalogFileWriterFactory;
import org.nrg.session.BeanBuilder;
import org.nrg.sl4fj.PrintWriterLogger;
import org.nrg.ulog.MicroLog;
import org.nrg.xdat.bean.XnatCrsessiondataBean;
import org.nrg.xdat.bean.XnatCtsessiondataBean;
import org.nrg.xdat.bean.XnatDxsessiondataBean;
import org.nrg.xdat.bean.XnatExperimentdataFieldBean;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMgsessiondataBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.XnatNmsessiondataBean;
import org.nrg.xdat.bean.XnatOptsessiondataBean;
import org.nrg.xdat.bean.XnatOtherdicomsessiondataBean;
import org.nrg.xdat.bean.XnatPetmrsessiondataBean;
import org.nrg.xdat.bean.XnatPetsessiondataBean;
import org.nrg.xdat.bean.XnatRtsessiondataBean;
import org.nrg.xdat.bean.XnatSrsessiondataBean;
import org.nrg.xdat.bean.XnatUssessiondataBean;
import org.nrg.xdat.bean.XnatXasessiondataBean;
import org.nrg.xdat.model.XnatAddfieldI;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.model.XnatMrscandataI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Sets;

/**
 * Generates an XNAT session metadata XML from a DICOM study.
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class DICOMSessionBuilder extends org.nrg.session.SessionBuilder implements Closeable {
    private static final String XML_SUFFIX = ".xml";

    private static final SortedSet<DicomAttributeIndex> ALL_TAGS;

    // Helpers for building subbeans
    private static final ImmutableMap<String,? extends BeanBuilder> imageSessionBeanBuilders = ImmutableMap.of("fields/field",
            new BeanBuilder() {
        public Collection<XnatExperimentdataFieldBean> buildBeans(final ExtAttrValue value) {
            final XnatExperimentdataFieldBean field = new XnatExperimentdataFieldBean();
            final String text = value.getText();
            if (Strings.isNullOrEmpty(text) || "null".equals(text)) {
                return Collections.emptyList();
            } else {
                field.setField(text);
                field.setName(value.getAttrs().get("name"));
                return Collections.singletonList(field);
            }
        }
    });

    private final Logger logger = LoggerFactory.getLogger(DICOMSessionBuilder.class);

    private final DicomMetadataStore store;
    private final File fsdir;
    private final String studyInstanceUID;

    private Integer nScans = null;
    private final MutableAttrDefs sessionAttrDefs = new MutableAttrDefs();
    private final MutableAttrDefs scanAttrDefs = new MutableAttrDefs();
    private final RelativePathWriterFactory catalogWriterFactory;

    private final static Map<Class<? extends XnatImagesessiondataBean>,AttrDefs> sessionTypeAttrs = Maps.newHashMap();
    private final static Map<Class<? extends XnatImagesessiondataBean>,Map<String,BeanBuilder>> sessionBeanBuilders = Maps.newHashMap();

    private final static String UNKNOWN_MODALITY = "OT";  // DICOM: Other

    static {
        sessionTypeAttrs.put(XnatMrsessiondataBean.class, MRSessionAttributes.get());
        sessionBeanBuilders.put(XnatMrsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatPetsessiondataBean.class, PETSessionAttributes.get());
        sessionBeanBuilders.put(XnatPetsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatPetmrsessiondataBean.class, PETMRSessionAttributes.get());
        sessionBeanBuilders.put(XnatPetmrsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatCtsessiondataBean.class, CTSessionAttributes.get());
        sessionBeanBuilders.put(XnatCtsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatXasessiondataBean.class, XASessionAttributes.get());
        sessionBeanBuilders.put(XnatXasessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatUssessiondataBean.class, USSessionAttributes.get());
        sessionBeanBuilders.put(XnatUssessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatRtsessiondataBean.class, RTSessionAttributes.get());
        sessionBeanBuilders.put(XnatRtsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatCrsessiondataBean.class, CRSessionAttributes.get());
        sessionBeanBuilders.put(XnatCrsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatOptsessiondataBean.class, OPTSessionAttributes.get());
        sessionBeanBuilders.put(XnatOptsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatMgsessiondataBean.class, MGSessionAttributes.get());
        sessionBeanBuilders.put(XnatMgsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatDxsessiondataBean.class, DXSessionAttributes.get());
        sessionBeanBuilders.put(XnatDxsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatNmsessiondataBean.class, NMSessionAttributes.get());
        sessionBeanBuilders.put(XnatNmsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatSrsessiondataBean.class, SRSessionAttributes.get());
        sessionBeanBuilders.put(XnatSrsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        sessionTypeAttrs.put(XnatOtherdicomsessiondataBean.class, OtherDICOMSessionAttributes.get());
        sessionBeanBuilders.put(XnatOtherdicomsessiondataBean.class, ImmutableMap.copyOf(imageSessionBeanBuilders));

        final SortedSet<DicomAttributeIndex> tags = newAttributeIndexSortedSet();
        tags.add(SOPClassUID);
        tags.add(SeriesNumber);
        tags.add(StudyInstanceUID);
        tags.add(SeriesInstanceUID);
        tags.add(Modality);
        tags.addAll(CatalogAttributes.get().getNativeAttrs());
        tags.addAll(ImageFileAttributes.get().getNativeAttrs());
        for (final AttrDefs attrDefs : sessionTypeAttrs.values()) {
            tags.addAll(attrDefs.getNativeAttrs());
        }
        tags.addAll(DICOMScanBuilder.getNativeTypeAttrs());
        ALL_TAGS = Collections.unmodifiableSortedSet(tags);
    }


    private static final Comparator<DicomAttributeIndex> COMPARATOR = new DicomAttributeIndex.Comparator();
    private static SortedSet<DicomAttributeIndex> newAttributeIndexSortedSet() {
        return Sets.newTreeSet(null == COMPARATOR ? new DicomAttributeIndex.Comparator() : COMPARATOR);
    }

    private final List<Class<? extends XnatImagesessiondataBeanFactory>> sessionBeanFactoryClasses = Lists.newArrayList();

    @SuppressWarnings("unchecked")
    private final static List<Class<? extends XnatImagesessiondataBeanFactory>> DEFAULT_SESSIONDATABEAN_FACTORIES = Lists.newArrayList(
            XnatPetmrImagesessiondataBeanFactory.class,
            SOPMapXnatImagesessiondataBeanFactory.class,
            ModalityMapXnatImagesessiondataBeanFactory.class);
    

    /**
     * Creates a session builder that sends output to the given Writer.
     * @param fsdir Root directory of the DICOM fileset
     * @param writer Where the XML session document gets written;
     *   if null, the document is written to a file with the same name as the root directory,
     *   plus the .xml suffix
     * @param sessionAttrDefs additional definitions for session-level attributes
     * @param scanAttrDefs additional definitions for scan-level attributes
     * @throws IOException
     */
    public DICOMSessionBuilder(final DicomMetadataStore store, final String studyInstanceUID,
            final File fsdir, final Writer writer,
            final Collection<XnatAttrDef> sessionAttrDefs,
            final Collection<XnatAttrDef> scanAttrDefs) throws IOException {
        super(fsdir, fsdir.getPath(), writer, new File(fsdir.getPath() + XML_SUFFIX));
        this.store = store;
        this.studyInstanceUID = studyInstanceUID;

        this.fsdir = fsdir.getCanonicalFile();
        this.sessionAttrDefs.addAll(sessionAttrDefs);
        this.scanAttrDefs.addAll(scanAttrDefs);
        this.catalogWriterFactory = new ScanCatalogFileWriterFactory(fsdir);
    }

    public DICOMSessionBuilder(final DicomMetadataStore store,
            final File fsdir, final Writer writer,
            final Collection<XnatAttrDef> sessionAttrDefs,
            final Collection<XnatAttrDef> scanAttrDefs)
                    throws IOException,NoUniqueSessionException,SQLException {
        this(store, getStudyInstanceUID(store), fsdir, writer, sessionAttrDefs, scanAttrDefs);
    }

    @SuppressWarnings("unchecked")
    public DICOMSessionBuilder(final File fsdir, final Writer writer,
            final Collection<XnatAttrDef> sessionAttrDefs,
            final Collection<XnatAttrDef> scanAttrDefs)
                    throws IOException,NoUniqueSessionException,SQLException {
        this(getStore(fsdir, getTags(sessionAttrDefs, scanAttrDefs)),
                fsdir, writer,
                sessionAttrDefs, scanAttrDefs);
    }

    @SuppressWarnings("unchecked")
    public DICOMSessionBuilder(final File fsdir, final XnatAttrDef[]sessionAttrs, final Function<DicomObject,DicomObject> fn) 
            throws IOException,NoUniqueSessionException,SQLException {
        this(getStore(fsdir, getTags(Arrays.asList(sessionAttrs)), fn),fsdir, null,Arrays.asList(sessionAttrs),Collections.<XnatAttrDef>emptyList());
    }

    /**
     * Creates a session builder that sends output to the given Writer.
     * @param fsdir Root directory of the DICOM fileset
     * @param writer Where the XML session document gets written
     * @param sessionAttrs additional definitions for session-level attributes
     * @throws IOException
     */
    public DICOMSessionBuilder(final File fsdir, final Writer writer, final XnatAttrDef...sessionAttrs)
            throws IOException,NoUniqueSessionException,SQLException {
        this(fsdir, writer, Arrays.asList(sessionAttrs), Collections.<XnatAttrDef>emptyList());
    }

    /**
     * Creates a session builder that writes output to a file with the
     * same name as the file set root, plus a '.xml' suffix.
     * @param fsdir Root directory of the DICOM fileset
     * @param sessionAttrs additiona definitions for session-level attributes
     * @throws IOException
     */
    public DICOMSessionBuilder(final File fsdir, final XnatAttrDef...sessionAttrs)
            throws IOException,NoUniqueSessionException,SQLException {
        this(fsdir, null, sessionAttrs);
    }

    private static String getStudyInstanceUID(final DicomMetadataStore store)
            throws IOException,NoUniqueSessionException,SQLException {
        try {
            final Set<String> uids = store.getUniqueValues(StudyInstanceUID);
            if (uids.isEmpty()) {
                throw new NoUniqueSessionException(new String[0]);
            }
            final Iterator<String> i = uids.iterator();
            final String uid = i.next();
            if (i.hasNext()) {
                throw new NoUniqueSessionException(uids.toArray(new String[0]));
            }
            return uid;
        } catch (ConversionFailureException e) {
            throw new RuntimeException(e);    // UIDs should require no conversion
        }
    }

    private static EnumeratedMetadataStore createStore(final File root, final Collection<DicomAttributeIndex> extraTags) 
            throws IOException,SQLException {
        final SortedSet<DicomAttributeIndex> tags = newAttributeIndexSortedSet();
        tags.addAll(ALL_TAGS);
        tags.addAll(extraTags);
        return EnumeratedMetadataStore.createHSQLDBBacked(tags);
    }

    private static DicomMetadataStore getStore(final File root, final Collection<DicomAttributeIndex> extraTags)
            throws IOException,SQLException {
        final EnumeratedMetadataStore s = createStore(root,extraTags);
        s.addFileFilter(new XNATSessionDICOMScanFilter(root));
        s.add(Collections.singleton(root));
        return s;
    }

    private static DicomMetadataStore getStore(final File root, final Collection<DicomAttributeIndex> extraTags,
            Function<DicomObject,DicomObject> op) 
                    throws IOException, SQLException {
        final EnumeratedMetadataStore s = createStore(root, extraTags);
        s.addFileFilter(new XNATSessionDICOMScanFilter(root));
        s.add(Collections.singleton(root),op);
        return s;
    }

    private static SortedSet<DicomAttributeIndex> getTags(final Collection<XnatAttrDef>...attrDefs) {
        final SortedSet<DicomAttributeIndex> tags = newAttributeIndexSortedSet();
        for (final Collection<XnatAttrDef> defs : attrDefs) {
            for (final XnatAttrDef def : defs) {
                tags.addAll(def.getAttrs());
            }
        }
        return tags;
    }


    public static SortedSet<DicomAttributeIndex> getAttributeTags() {
        return ALL_TAGS;
    }

    /**
     * Creates a session bean object for the named study
     * @param store
     * @param ulog
     * @return
     * @throws IOException
     * @throws SQLException
     */
    private XnatImagesessiondataBean makeSessionBean(final Iterable<XnatImagesessiondataBeanFactory> factories,
            final String studyInstanceUID,
            final Logger sessionLogger)
                    throws IOException,SQLException {
        // Try complex and custom rules first
        for (final XnatImagesessiondataBeanFactory factory : factories) {
            final XnatImagesessiondataBean sessionbean = factory.create(store, studyInstanceUID);
            if (null != sessionbean) {
                return sessionbean;
            }
        }

        // Nothing worked; try to explain what happened.
        final Map<DicomAttributeIndex,ConversionFailureException> failures = Maps.newLinkedHashMap();
        final SetMultimap<DicomAttributeIndex,String> vals = store.getUniqueValuesGiven(ImmutableMap.of(StudyInstanceUID,studyInstanceUID),
                Arrays.asList(SOPClassUID, Modality), failures);
        for (final Map.Entry<DicomAttributeIndex,ConversionFailureException> fme : failures.entrySet()) {
            sessionLogger.error("unable to convert attribute " + fme.getKey(), fme.getValue());
        }

        sessionLogger.error("Session builder not implemented for SOP class {} or modality {}", vals.get(SOPClassUID), vals.get(Modality));
        return null;
    }

    /**
     * Append to the list of session bean factory classes.
     * @param classes session bean factory classes to append
     * @return this
     */
    public DICOMSessionBuilder
    setSessionBeanFactories(Iterable<Class<? extends XnatImagesessiondataBeanFactory>> classes) {
        sessionBeanFactoryClasses.clear();
        Iterables.addAll(sessionBeanFactoryClasses, classes);
        return this;
    }
    
    /**
     * Build the session bean factories.
     * Each class must have a constructor of form SessionBeanFactoryClass(org.slf4j.Logger)
     *
     * @param classes Session bean factory classes
     * @param logger logger to which each factory class will report
     * @return session bean factory instances
     */
    private static List<XnatImagesessiondataBeanFactory>
    makeSessionBeanFactories(final List<Class<? extends XnatImagesessiondataBeanFactory>> classes,
            final Logger logger) {
        if (classes.isEmpty()) {
            classes.addAll(DEFAULT_SESSIONDATABEAN_FACTORIES);
        }
        
        final List<XnatImagesessiondataBeanFactory> factories = Lists.newArrayList();
        for (final Class<? extends XnatImagesessiondataBeanFactory> clazz : classes) {            
            try {
                factories.add(clazz.getConstructor(Logger.class).newInstance(logger));
            } catch (final RuntimeException e) {
                throw e;
            } catch (final Exception e) {
                // Every possible exception here is programmer error; don't use checked exceptions.
                throw new RuntimeException(e);
            }
        }
        return factories;
    }
    

    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    public XnatImagesessiondataBean call()
            throws IOException,NoUniqueSessionException,SQLException {
        logger.info("Building session for {}", studyInstanceUID);
        final PrintWriterLogger sessionLog = new PrintWriterLogger(new File(fsdir, "dcmtoxnat.log"));
        final List<XnatImagesessiondataBeanFactory> sessionBeanFactories = makeSessionBeanFactories(sessionBeanFactoryClasses, sessionLog);
        try {
            final XnatImagesessiondataBean session = makeSessionBean(sessionBeanFactories, studyInstanceUID, logger);
            if (null == session) {
                throw new NoUniqueSessionException("no session type identified");
            }
            final AttrAdapter sessionAttrs = new AttrAdapter(store,
                    Collections.singletonMap(StudyInstanceUID, studyInstanceUID));
            sessionAttrs.add(sessionAttrDefs);
            sessionAttrs.add(sessionTypeAttrs.get(session.getClass()));

            final Map<ExtAttrDef<DicomAttributeIndex>,Throwable> failures = Maps.newHashMap();
            final List<ExtAttrValue> sessionValues = getValues(sessionAttrs, failures);
            for (final Map.Entry<ExtAttrDef<DicomAttributeIndex>,Throwable> me: failures.entrySet()) {
                if ("UID".equals(me.getKey().getName())) {
                    final Throwable cause = me.getValue();
                    if (cause instanceof NoUniqueValueException) {
                        throw new NoUniqueSessionException(((NoUniqueValueException)cause).getValues());
                    } else {
                        throw new RuntimeException("Unable to derive UID for unexpected cause", cause);
                    }
                } else {
                    report("session", me.getKey(), me.getValue(), sessionLog);
                }
            }

            // prearchivePath is a special case; we set this by hand.
            for (final ExtAttrValue val : setValues(session, sessionValues,
                    sessionBeanBuilders.get(session.getClass()), "prearchivePath")) {
                final String name = val.getName();
                if ("prearchivePath".equals(name)) {
                    setPrearchivePath(session);
                } else {
                    throw new RuntimeException("attribute " + val.getName() + " unexpectedly skipped");
                }
            }

            // Identify the scans -- one scan per value of Series Instance UID
            final SortedMap<String,Map<String,Series>> seriesToUID = Maps.newTreeMap(new Utils.MaybeNumericStringComparator());
            final Set<Map<DicomAttributeIndex,String>> seriesIds;
            final Map<DicomAttributeIndex,ConversionFailureException> failed = Maps.newHashMap();
            seriesIds = store.getUniqueCombinations(Arrays.asList(SeriesNumber, SeriesInstanceUID, SOPClassUID, Modality), failed);
            if (!failed.isEmpty()) {
                sessionLog.log("Unable to retrieve some series-identifying attributes: " + failed);
            }

            for (final Map<DicomAttributeIndex,String> entry : seriesIds) {
                final String seriesInstanceUID = entry.get(SeriesInstanceUID);
                String seriesNumber = entry.get(SeriesNumber);
                if (Strings.isNullOrEmpty(seriesNumber)) {
                    seriesNumber = seriesInstanceUID.replaceAll("[^\\w]", "_");
                }
                final Map<String,Series> uidToSeries;
                if (seriesToUID.containsKey(seriesNumber)) {
                    uidToSeries = seriesToUID.get(seriesNumber);
                } else {
                    uidToSeries = Maps.newTreeMap();
                    seriesToUID.put(seriesNumber, uidToSeries);
                }

                if (!uidToSeries.containsKey(seriesInstanceUID)) {
                    uidToSeries.put(seriesInstanceUID, new Series(seriesNumber, seriesInstanceUID));
                }
                final Series series = uidToSeries.get(seriesInstanceUID);
                series.addModality(entry.get(Modality));
                series.addSOPClass(entry.get(SOPClassUID));
            }

            final Map<String,Series> scanToSeries = Maps.newLinkedHashMap();
            for (final Map.Entry<String,Map<String,Series>> nuse : seriesToUID.entrySet()) {  // Number->UID->Series entry
                final String seriesNumber = nuse.getKey();
                final Map<String,Series> uids = nuse.getValue();
                assert !uids.isEmpty();
                if (uids.size() > 1) {  // each study ID looks like "{SeriesNum}-{Modality}{index}"
                    final Map<String,Integer> modalityCounts = Maps.newHashMap();
                    for (final Series series : uids.values()) {
                        String modality = SOPModel.getLeadModality(series.getModalities());
                        if (null == modality) {
                            modality = UNKNOWN_MODALITY;
                        }
                        final int index;
                        if (modalityCounts.containsKey(modality)) {
                            index = modalityCounts.get(modality) + 1;
                            modalityCounts.put(modality, index);
                        } else {
                            index = 1;
                            modalityCounts.put(modality, index);
                        }
                        final String scanID = new StringBuilder(seriesNumber).append("-").append(modality).append(index).toString();
                        scanToSeries.put(scanID, series);
                    }
                } else {
                    // Just one Series Instance UID for this Series Number; use Series Number as Scan ID
                    scanToSeries.put(seriesNumber, uids.values().iterator().next());
                }
            }

            nScans = scanToSeries.size();

            for (final Map.Entry<String,Series> nse : scanToSeries.entrySet()) {  // Number->Series entry
                try {
                    final Map<DicomAttributeIndex,String> scanSpec = ImmutableMap.of(SeriesInstanceUID, nse.getValue().getUID());
                    final DICOMScanBuilder scanBuilder = DICOMScanBuilder.fromStore(store, sessionLog,
                            nse.getKey(), nse.getValue(), scanSpec, catalogWriterFactory, useRelativePaths());
                    session.addScans_scan(scanBuilder.call());
                } catch (Throwable t) {
                    logger.info("Unable to process scan " + nse.getKey(), t);
                    sessionLog.log("Unable to process scan " + nse.getKey(), t);
                }
            }
            
            hcpScanPostprocess(session.getScans_scan());
            
            return session;
        } finally {
            if (sessionLog.hasMessages()) {
                logger.info("Warnings occured in processing " + fsdir.getPath() + "; see " + sessionLog);
                sessionLog.close();
            }
        }
    }
    
    private static final String HCP_UNUSABLE_QUALITY = "unusable";
    private static final Pattern RR_PATTERN = Pattern.compile("(.*?)(_SBRef)?");
    private static final Pattern PRESERVE_SERIES_DESC_PATTERN = Pattern.compile("BOLD_(2mm_)?(LR|RL)_SB_SE");

    /*
     *  For a retro recon series (defined as one with _RR suffix in Series Description),
     *  the XNAT seriesDescription gets renamed to remove _RR. Any other scans with that
     *  same name (without _RR) should be renamed to {SeriesDescription}_old
     */
    private final void hcpScanPostprocess(final Iterable<XnatImagescandataI> scans) {
        for (final XnatImagescandataI scan : scans) {
            if (rrLabelWasRemoved(scan)) {
                final String desc = scan.getSeriesDescription();
                if (desc.contains("_RR")) {
                    logger.warn("scan {} has had _RR removed but still includes _RR!", scan);
                }
                for (final XnatImagescandataI other : scans) {
                    if (Objects.equal(desc, other.getSeriesDescription()) &&
                            !PRESERVE_SERIES_DESC_PATTERN.matcher(desc).matches() &&
                            !rrLabelWasRemoved(other)) {
                        final Matcher m = RR_PATTERN.matcher(desc);
                        if (m.matches()) {
                            final StringBuilder sb = new StringBuilder();
                            if (!Strings.isNullOrEmpty(m.group(1))) {
                                sb.append(m.group(1));
                            }
                            sb.append("_old");
                            if (!Strings.isNullOrEmpty(m.group(2))) {
                                sb.append(m.group(2));
                            }
                            other.setSeriesDescription(sb.toString());
                        } else {
                            logger.warn("scan {} misses trivial pattern match?", desc);
                            other.setSeriesDescription(desc + "_old");
                        }
                        other.setQuality(HCP_UNUSABLE_QUALITY);
                    }
                }
            }
        }
        
    }
    
   private final boolean rrLabelWasRemoved(final XnatImagescandataI scan) {
        if (scan instanceof XnatMrscandataI) {
            for (final XnatAddfieldI addf : ((XnatMrscandataI)scan).getParameters_addparam()) {
                if (HCPSeriesDescription.RR_LABEL_REMOVED.equals(addf.getName()) &&
                        Boolean.parseBoolean(addf.getAddfield())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public final String getSessionInfo() {
        final StringBuilder sb = new StringBuilder();
        sb.append(null == nScans ? "(undetermined)" : nScans);
        sb.append(" scans");
        return sb.toString();
    }

    public final void close() throws IOException {
        store.close();
    }

    private static StringBuilder startReport(final Object context, final ExtAttrDef<?> subject) {
        return new StringBuilder(context.toString()).append(" attribute ").append(subject.getName());
    }

    static void report(final Object context, final ExtAttrDef<?> subject, final Throwable t, final MicroLog log) {
        try {
            if (t instanceof ConversionFailureException) {
                report(context, subject, (ConversionFailureException)t, log);
            } else if (t instanceof NoUniqueValueException) {
                report(context, subject, (NoUniqueValueException)t, log);
            } else {
                final StringBuilder sb = startReport(context, subject);
                sb.append(" could not be resolved :").append(t);
                log.log(sb.toString());
            }
        } catch (IOException e1) {
            LoggerFactory.getLogger(DICOMSessionBuilder.class).error("Unable to write to session log " + log, e1);
        }
    }

    static void report(final Object context, final ExtAttrDef<?> subject,
            final ConversionFailureException e, final MicroLog log) throws IOException {
        final StringBuilder sb = startReport(context, subject);
        sb.append(": DICOM attribute ").append(e.getAttr());
        sb.append(" has bad value (").append(e.getValue()).append("); unable to derive attribute(s) ");
        sb.append(Arrays.toString(e.getExtAttrs()));
        log.log(sb.toString());
    }

    private static void report(final Object context, final ExtAttrDef<?> subject,
            final NoUniqueValueException e, final MicroLog log) throws IOException {
        final StringBuilder sb = startReport(context, subject);
        if (0 == e.getValues().length) {
            sb.append(" has no value");
        } else {
            sb.append(" has multiple values: ").append(Arrays.toString(e.getValues()));
        }
        log.log(sb.toString());
    }


    static File getCommonRoot(final Iterable<File> files) {
        final Iterator<File> fi = files.iterator();
        File root = fi.next().getParentFile();
        while (fi.hasNext()) {
            root = getCommonRoot(root, fi.next());
        }
        return root;
    }

    private static File getCommonRoot(final File f1, final File f2) {
        if (null == f1) return null;
        for (File f2p = f2.getParentFile(); null != f2p; f2p = f2p.getParentFile()) {
            if (f1.equals(f2p)) return f1;
        }
        return getCommonRoot(f1.getParentFile(), f2);
    }

    private static final String PROJECT_PREFIX = "Project:";
    private static final String SUBJECT_PREFIX = "Subject:";
    private static final String SESSION_PREFIX = "Session:";

    private static final String PROJECT_SPEC_LABEL = "Project";
    private static final String SUBJECT_SPEC_LABEL = "Subject";
    private static final String SESSION_SPEC_LABEL = "Session";
    private static final String ASSIGNMENT = "\\:";
    private static final String LABEL_PATTERN = "\\w+";

    /**
     * Runs the session XML builder on the named files.
     * Uses properties to control behavior:
     * output.dir is the pathname of a directory where XML should be saved
     * @param args roots (directory pathnames) for which XML should be written
     */
    public static void main(String[] args) throws IOException,NoUniqueSessionException,SQLException {
        final XnatAttrDef defaultProjectAttrDef = new ConditionalAttrDef("project",
                new Rule[] {
                new ContainsAssignmentRule(PatientComments,
                        PROJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                        new ContainsAssignmentRule(StudyComments,
                                PROJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                                new EqualsRule(StudyDescription),
                                new EqualsRule(AccessionNumber),

        });

        final XnatAttrDef defaultSubjectAttrDef = new ConditionalAttrDef("subject_ID",
                new Rule[] {
                new ContainsAssignmentRule(PatientComments,
                        SUBJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                        new ContainsAssignmentRule(StudyComments,
                                SUBJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                                new EqualsRule(PatientName)
        });

        final XnatAttrDef defaultSessionAttrDef = new ConditionalAttrDef("label",
                new Rule[] {
                new ContainsAssignmentRule(PatientComments,
                        SESSION_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                        new ContainsAssignmentRule(StudyComments,
                                SESSION_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                                new EqualsRule(PatientID)
        });

        final String outputRootName = System.getProperty("output.dir");
        final File outputRoot = (outputRootName == null) ? null : new File(outputRootName);
        if (outputRoot != null && !outputRoot.isDirectory()) {
            System.err.println("output.dir must be a directory");
            System.exit(-1);
        }

        if (4 < args.length) {
            System.out.println("Usage: SessionBuilder dicom-dir [Project:project] "
                    + "[Subject:subject_id] [Session:session_id]");
            System.exit(-1);
        }

        for (int i = 0; i < args.length; ) {
            final File f = new File(args[i++]);
            final File out = (outputRoot == null) ? new File(f.getPath() + ".xml") : new File(outputRoot, f.getName() + ".xml");
            XnatAttrDef project = defaultProjectAttrDef;
            XnatAttrDef subject = defaultSubjectAttrDef;
            XnatAttrDef session = defaultSessionAttrDef;
            while (i < args.length) {
                String nextArg = args[i++];
                if (nextArg.startsWith(PROJECT_PREFIX)) {
                    project = new XnatAttrDef.Constant("project", nextArg.substring(PROJECT_PREFIX.length()));
                } else if (nextArg.startsWith(SUBJECT_PREFIX)) {
                    subject = new XnatAttrDef.Constant("subject_ID", nextArg.substring(SUBJECT_PREFIX.length()));
                } else if (nextArg.startsWith(SESSION_PREFIX)) {
                    session = new XnatAttrDef.Constant("label", nextArg.substring(SESSION_PREFIX.length()));
                } else {
                    i--;  // not using nextArg here after all
                    break;
                }
            }
            LoggerFactory.getLogger(DICOMSessionBuilder.class).debug("starting");
            final DICOMSessionBuilder psb = new DICOMSessionBuilder(f, new FileWriter(out),
                    LabelAttrDef.wrap(project), LabelAttrDef.wrap(subject), LabelAttrDef.wrap(session));
            psb.setIsInPrearchive(Boolean.parseBoolean(System.getProperty("is.prearc", "true")));
            psb.run();
            psb.close();
        }
    }
}
