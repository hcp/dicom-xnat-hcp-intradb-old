/**
 * Copyright (c) 2008-2012 Washington University
 */
package org.nrg.dcm.xnat;


import org.dcm4che2.data.Tag;
import org.hcp.dcm.HCPSeriesDescription;
import org.nrg.attr.MinimumValueAttrDef;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class ImageScanAttributes {
	private ImageScanAttributes() {} // no instantiation

	static public AttrDefs get() { return s; }

	static final private MutableAttrDefs s = new MutableAttrDefs();

	static {
		s.add("ID");    // handled by session builder
		s.add("UID", Tag.SeriesInstanceUID);

		s.add(new HCPSeriesDescription());

		s.add("scanner", Tag.StationName);
		s.add("scanner/manufacturer", Tag.Manufacturer);
		s.add("scanner/model", Tag.ManufacturerModelName);
		s.add("scanner/softwareVersion", Tag.SoftwareVersions);
		
		// Use the earliest acquisition time as the scan start time
		// Default XNAT uses ScanStartTimeAttrDef, which also checks Series Time,
		// but Siemens scanners mess up the time attributes on recon scans. Using
		// the Acquisition Time alone is the least bad of bad options for HCP.
		s.add(MinimumValueAttrDef.wrap(new AcquisitionTimeAttribute("startTime")));
		// s.add(MaximumValueAttrDef.wrap(new AcquisitionTimeAttribute("endTime")));
	}
}
