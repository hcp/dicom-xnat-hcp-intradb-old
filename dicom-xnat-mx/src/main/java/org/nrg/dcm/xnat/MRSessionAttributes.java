/**
 * Copyright (c) 2006-2012 Washington University
 */
package org.nrg.dcm.xnat;

import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

/**
 * mrSessionData attributes
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
final class MRSessionAttributes {
    private MRSessionAttributes() {}    // no instantiation
    static public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageSessionAttributes.get());
    static {
        s.add(new MagneticFieldStrengthAttribute());
    }
}
