/**
 * Copyright (c) 2009-2013 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.UID;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class OtherDICOMSessionAttributes {
    private OtherDICOMSessionAttributes() {}    // no instantiation
    static public AttrDefs get() { return s; }

    String foo = UID.SecondaryCaptureImageStorage;
    static final private MutableAttrDefs s = new MutableAttrDefs(ImageSessionAttributes.get());
}
