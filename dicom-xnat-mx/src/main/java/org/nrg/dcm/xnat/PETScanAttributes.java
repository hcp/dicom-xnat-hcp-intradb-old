/**
 * Copyright (c) 2009,2012 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class PETScanAttributes {
    private PETScanAttributes() {}  // no instantation

    static public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageScanAttributes.get());

    static {
        s.add("parameters/facility", Tag.InstitutionName);
    }
}
